<?php
/**
 * The header style two Aazeen Theme.
 * @package Aazeen
 * @since 1.0.0
 */
?>

<div class="header2 desktop-menu">
    <div class="banner-warp" <?php if ( get_header_image() ) : ?> data-interchange="[<?php echo esc_url( header_image());?>, small],[<?php echo esc_url( header_image());?>, large]" <?php endif;?>>
      <div class="grid-container">
        <div class="logo-inner">
          <?php the_custom_logo(); ?>
          <div class="site-branding">
            <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
            <?php $description = get_bloginfo( 'description', 'display' );
            if ( $description || is_customize_preview() ) : ?>
            <p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
            <?php endif; ?>
          </div><!-- .site-branding -->
          <?php $social_icons_top = get_theme_mod( 'social_icons_top'); ?>
          <?php if( !empty( $social_icons_top ) ):?>
            <div class="header-social-wrap">
              <div class="header-social-inner">
                <?php foreach( $social_icons_top as $row ) : ?>
                  <a <?php if ( true == get_theme_mod( 'open_social_tab', false ) ) : ?>target="_blank"<?php endif; ?> href="<?php echo esc_url($row['social_url']); ?>">
                    <button class=" btn btn-just-icon btn-<?php echo esc_attr( $row['social_icon']); ?>">
                      <i class="fa fa-<?php echo esc_attr( $row['social_icon']); ?>"></i>
                    </button>
                  </a>
                <?php endforeach; ?>
              </div>
            </div>
          <?php endif; ?>
        </div>
        <!--site-title END-->
      </div>
    </div>
<!--branding END-->
<div data-sticky-container="data-sticky-container">
  <?php if ( true == get_theme_mod( 'sticky_menu_onof', true ) ) : ?>
    <div class="menu-outer sticky_menu animated z-depth-1" data-sticky="data-sticky" data-options="marginTop:0;" style="width:100%" data-anchor="content">
    <?php else : ?>
      <div class="menu-outer z-depth-1">
      <?php endif; ?>
        <div class="top-bar">
              <div class="no-js main-menu-wrap float-center">
                <?php if ( has_nav_menu( 'primary' ) ) : ?>
                  <?php aazeen_top_nav(); ?>
                <?php else : ?>
                  <ul class="horizontal menu  desktop-menu dropdown align-center">
                    <li id="add-menu" class="menu-item">
                      <a href=" <?php echo esc_url(admin_url( 'nav-menus.php' ));?>  "><?php echo __( 'Add a Primary Menu', 'aazeen' );?>
                      </a>
                    </li>
                  </ul>
                <?php endif; ?>
              </div>
                <ul class="navbar-search">
                  <button class="navbar-search-button" data-toggle="navbar-search-bar navbar-filter-icons-container">
                    <i class="fa fa-search" aria-hidden="true"></i>
                  </button>
                </ul>
            </div>
            <div class="navbar-search-bar-container animated  " id="navbar-search-bar" data-toggler=".is-hidden" data-animate="fade-in fade-out" data-closable="" aria-expanded="true" style="display: none;">
              <?php get_search_form(); ?>
              <button class="close-button fast" data-close="">&times;</button>
            </div>
          </div>
        </div>
</div>
