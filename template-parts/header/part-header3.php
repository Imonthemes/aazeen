<?php
/**
 * The header style Three Aazeen Theme.
 * @package Aazeen
 * @since 1.0.0
 */
?>
<div class="header3   <?php if ( true == get_theme_mod( 'sticky_menu_onof', true ) ) : ?>
sticky sticky_menu <?php else :?> unsticky_menu <?php endif; ?>  desktop-menu"  <?php if ( get_header_image() ) : ?> data-interchange="[<?php echo esc_url( header_image());?>, small],[<?php echo esc_url( header_image());?>, large]" <?php endif;?>>
 <div class="top-bar">
  <div class="top-bar-left">
    <ul class="dropdown menu" >
      <li class="menu-text">
        <?php get_template_part( 'template-parts/menu/site', 'branding' ); ?>
      </li>
    </ul>
  </div>
  <div class="top-bar-right no-js">
    <?php get_template_part( 'template-parts/menu/main', 'menu' ); ?>
  </div>
  <div class="navbar-search-bar-container animated  " id="navbar-search-bar" data-toggler=".is-hidden" data-animate="fade-in fade-out" data-closable="" aria-expanded="true" style="display: none;">
    <?php get_search_form(); ?>
    <button class="close-button fast" data-close="">&times;</button>
  </div>
  </div>
</div>
