<?php
/**
 * post layout1 for displaying postpage
 *
 * @package themezwp
 * @subpackage aazeen
 * @since aazeen 1.0
 */
 ?>

<article class=" post-wrap-layout-1 radius ">
  <div class="media-object stack-for-small ">
    <?php if ( has_post_thumbnail() ) { ?>
    <div class="media-object-section align-self-middle ">
      <div class="thumbnail">
        <div class="post-thumb">
          <?php the_post_thumbnail( 'aazeen-post-layout1',array('class' => 'object-fit-images','link_thumbnail' =>TRUE)  ); ?>
        </div>
      </div>
    </div>
    <?php } ?>
      <div class="media-object-section post-wrap">
        <div class="post-header-warp">
          <div class="entry-category">
            <div class="meta-info-cat">
              <?php aazeen_category_list(); ?>
            </div>
          </div>
          <?php the_title( sprintf( '<h4 class="post-title"><a class="post-title-link" href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h4>' ); ?>
          <div class="entry-category">
            <?php echo aazeen_time_link(); ?>
          </div>
        </div>
        <div class="post-excerpt">
          <?php the_excerpt(); ?>
        </div>
      </div>
  </div>
</article>
