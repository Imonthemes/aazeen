<?php
/**
* Template part for displaying single posts header
*
* @link https://codex.wordpress.org/Template_Hierarchy
*
* @package aazeen
*/

?>
<?php
global $post;
$post_id = $post->ID; ?>
<?php
	$value_gradient = rwmb_meta( 'select_gradient_post_subheader' );
?>

	<div id="sub_single_page" class=" callout  border-none header-filter" data-parallax="active" >
		<div class="single-post-thumb-outer 	<?php if (! has_post_thumbnail( $post_id  ) ) : ?> <?php if (! $value_gradient) : ?> gradient_12 <?php else:?> gradient_<?php echo esc_attr($value_gradient);?> <?php endif;?> post-no-image <?php endif;?> ">
			<?php if ( has_post_thumbnail( $post_id  ) ) : ?>
				<div class="post-thumb-single "  >
					<?php echo get_the_post_thumbnail($post_id, 'aazeen-xlarge', array( 'class' => 'float-center object-fit-img' )); ?>
				</div>
			<?php endif;?>

			<div class="single-headwarp">
				<div class="grid-container">
					<h1><?php the_title($post_id); ?></h1>
					<div class="post-meta">
						<span class="font-bold label transparent meta-author">
							<?php global $post; $author_id=$post->post_author; ?>
							<span><?php echo esc_html__('By', 'aazeen');?> </span>
							<a class="vcard author" href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID', $author_id))); ?>" title="<?php echo esc_attr(the_author_meta( 'nickname', $author_id )); ?>">
								<?php echo the_author_meta( 'nickname', $author_id );?>
							</a>
						</span>
						<i class="fa fa-dot-circle-o" aria-hidden="true"></i>
						<span class="font-bold label transparent">
							<?php echo aazeen_time_link($post_id); ?>
						</span>
						<i class="fa fa-dot-circle-o" aria-hidden="true"></i>
						<span class="font-bold label transparent">
							<?php aazeen_meta_comment($post_id); ?>
						</span>
						<i class="fa fa-dot-circle-o" aria-hidden="true"></i>
						<span class="font-bold label  transparent">
							<?php aazeen_category_list($post_id); ?>
						</span>
					</div>
				</div>
			</div>
		</div>
</div>
