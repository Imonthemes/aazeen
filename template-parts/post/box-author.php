<?php
/**
 *
 * Displays Author Box
 *
 * @package aazeen
 *
 * @since aazeen 1.0.0
 */
;?>


<div class="single-box-author ">
  <div class="media-object stack-for-small transparent">
    <div class="media-object-section align-self-middle">
      <div class="author-thumb-wrap radius z-depth-1">
        <?php echo get_avatar(get_the_author_meta('ID'), '200'); ?>
      </div>
    </div>

        <div class="media-object-section box-author-warp large-text-left ">
          <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" title="<?php echo esc_attr( get_the_author() ); ?>">
            <h3><?php echo get_the_author();?></h3>
          </a>
        <div class="author-description">
          <?php the_author_meta( 'description' ); ?>
        </div>
            <a class="floating-action  raised-button button secondary radius font-bold" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" title="<?php echo esc_attr( get_the_author() ); ?>">
              <?php printf( esc_html__( 'View all posts', 'aazeen' ), esc_attr( get_the_author() ) ); ?>
            </a>
        </div>
  </div>
</div>
