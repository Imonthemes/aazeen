<?php
/**
* Template part for displaying posts
*
* @link https://codex.wordpress.org/Template_Hierarchy
*
* @package aazeen
*/

?>
<div class="content-wrapper" >
	<?php get_template_part('template-parts/post/single', 'head');?>

	<div class="grid-container single-main card padding-top-2 ">
		<div class="grid-x grid-margin-x align-center card-section">
			<?php if (have_posts()): ?>
				<?php while (have_posts()): ?>
					<?php the_post(); ?>
					<div class="cell small-24 <?php if ( ! is_active_sidebar( 'right-sidebar' ) ) : ?> large-22 <?php else:?> large-17 <?php endif;?> ">
						<article class="single-post-warp" id="post-<?php the_ID(); ?> ">
							<div class="post-single-content-body">
								<?php
								the_content(sprintf(
									/* translators: %s: Name of current post. */
									wp_kses(__('Continue reading %s <span class="meta-nav">&rarr;</span>', 'aazeen'), array( 'span' => array( 'class' => array() ) )),
									the_title('<span class="screen-reader-text">"', '"</span>', false)
								));
								wp_link_pages(array(
									'before' => '<div class="page-links">' . esc_html__('Pages:', 'aazeen'),
									'after'  => '</div>',
								));
								?>
								</div>
							<!-- post single content body END -->
						</article>
						<hr />
						<div class="single-nav transparent font-bold clearfix" role="navigation" id="sidebar-anchor-btm">
							<?php
							the_post_navigation(array(
								'prev_text' => '<span class="screen-reader-text">' . __('Previous Post', 'aazeen') . '</span><span class="aazeen-nav-icon nav-left-icon"><i class="fa fa-angle-left"></i></span><span class="nav-left-link">%title</span>',
								'next_text' => ' <span class="screen-reader-text">' . __('Next Post', 'aazeen') . '</span><span class="nav-right-link">%title</span><span class="newspaper-nav-icon nav-right-icon"><i class="fa fa-angle-right"></i></span>',
							));?>
						</div>
						<hr />
						<?php get_template_part('template-parts/post/box', 'author');?>
					<?php if (comments_open() || get_comments_number()) {?>
						<div class="box-comment-content ">
							<div class="grid-x grid-padding-x align-center">
								<div class="cell large-22 medium-24 small-24">
    								<?php comments_template();?>
								</div>
							</div>
						</div>
					<?php }?>
					<?php get_template_part('template-parts/post/related', 'post');?>
					</div>
				<?php endwhile ?>
			<?php endif ?>
			<!-- End of the loop. -->
			<?php get_template_part('sidebar'); ?>
		</div>
	</div>
</div>
