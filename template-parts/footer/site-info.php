<?php
/**
 * Displays footer site info
 *
 * themezwp
 * @subpackage aazeen
 * @since 1.0
 * @version 1.0
 */

?>
<!--COPYRIGHT TEXT-->
<div id="footer-copyright" class="footer-copyright-wrap">
	<?php $aazeen_footertext = get_theme_mod ('aazeen_footertext');?>
	<div class="grid-container">
		<div class="callout margin-vertical-0 border-none copy-text">
			<span><?php echo html_entity_decode(esc_textarea($aazeen_footertext));?></span>
			<a target="_blank" href="<?php echo esc_url( 'http://themezwp.com/'); ?>"><?php printf( esc_attr__( 'Theme by %s', 'aazeen' ), 'themezwp' ); ?></a>
		</div>
	</div>
</div>
