<?php
/**
 * Displays footer widgets if assigned
 *
 * themezwp
 * @subpackage aazeen
 * @since 1.0
 * @version 1.0
 */

?>
<?php if ( is_active_sidebar( 'foot_sidebar_full' ) ) { ?>
  <!--FOOTER WIDGETS-->
  <div class="top-footer-wrap-full">
    <div class="grid-container full">
      <div class="grid-x grid-padding-y align-center ">
        <?php if ( is_active_sidebar('dynamic_sidebar') || !dynamic_sidebar('foot_sidebar_full') ) : ?><?php endif; ?>
      </div>
    </div>
  </div>
  <!--FOOTER WIDGETS END-->
<?php } ?>
<?php if ( is_active_sidebar( 'foot_sidebar' ) ) { ?>
  <!--FOOTER WIDGETS-->
  <div class="top-footer-wrap padding-vertical-2">
    <div class="grid-container">
      <div class="grid-x grid-padding-x align-center ">
        <?php if ( is_active_sidebar('dynamic_sidebar') || !dynamic_sidebar('foot_sidebar') ) : ?><?php endif; ?>
      </div>
    </div>
  </div>
  <!--FOOTER WIDGETS END-->
<?php } ?>
