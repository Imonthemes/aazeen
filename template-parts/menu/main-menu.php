<?php
/**
 * The TOP header
 */
?>
	<div class="main-menu-wrap ">
		<?php if ( has_nav_menu( 'primary' ) ) : ?>
			<ul class="menu">
			<?php aazeen_top_nav(); ?>
			<?php $social_icons_top = get_theme_mod( 'social_icons_top'); ?>
			<?php if( !empty( $social_icons_top ) ):?>
				<li class=" menu">
				<div class="header-social-wrap">
					<div class="header-social-inner">
						<?php foreach( $social_icons_top as $row ) : ?>
							<a <?php if ( true == get_theme_mod( 'open_social_tab', false ) ) : ?>target="_blank"<?php endif; ?> href="<?php echo esc_url($row['social_url']); ?>">
								<button class=" btn btn-just-icon btn-<?php echo esc_attr( $row['social_icon']); ?>">
									<i class="fa fa-<?php echo esc_attr( $row['social_icon']); ?>"></i>
								</button>
							</a>
						<?php endforeach; ?>
					</div>
				</div>
				</li>
			<?php endif; ?>

			<li class="navbar-search menu">
				<button class="navbar-search-button" data-toggle="navbar-search-bar navbar-filter-icons-container">
					<i class="fa fa-search" aria-hidden="true"></i>
				</button>
			</li>
			</ul>
		<?php else : ?>
			<ul class="horizontal menu  desktop-menu dropdown align-center">
				<?php if ( current_user_can( 'edit_theme_options' )  ) : ?>
				<li id="add-menu" class="menu-item">
					<a href=" <?php echo esc_url(admin_url( 'nav-menus.php' ));?>  "><?php echo __( 'Add a Primary Menu', 'aazeen' );?>
					</a>
				</li>
				<?php endif; ?>
			</ul>
		<?php endif; ?>
	</div>
