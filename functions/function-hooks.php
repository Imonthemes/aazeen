<?php
global $post;

/**
* Use front-page.php when Front page displays is set to a static page.
*
* @since aazeen 1.0
*
* @param string $template front-page.php.
*
* @return string The template to be used: blank if is_home() is true (defaults to index.php), else $template.
*/
function aazeen_front_page_template($template)
{
    return is_home() ? '' : $template;
}
add_filter('frontpage_template', 'aazeen_front_page_template');

/**
* Filter the except length to 40 characters.
*
* @param int $length Excerpt length.
* @return int (Maybe) modified excerpt length.
*/
function aazeen_custom_excerpt_length($length)
{
  if ( is_admin() ) {
    return $length;
  }

    return 40;
}
add_filter('excerpt_length', 'aazeen_custom_excerpt_length', 999);

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * @since aazeen 1.0
 *
 * @param string $link Link to single post/page.
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function  aazeen_excerpt_more( $link ) {
	if ( is_admin() ) {
		return $link;
	}

	$link = sprintf( '<p class="link-more"><a href="%1$s" class="btn btn-primary btn-sm more-link">%2$s</a></p>',
		esc_url( get_permalink( get_the_ID() ) ),
		/* translators: %s: Name of current post */
		sprintf( __( 'Read more<span class="screen-reader-text"> "%s"</span>', 'aazeen' ), get_the_title( get_the_ID() ) )
	);
	return ' &hellip; ' . $link;
}
add_filter( 'excerpt_more', 'aazeen_excerpt_more' );



/**
 * Link thumbnails to their posts based on attr
 *
 * @param $html
 * @param int $pid
 * @param int $post_thumbnail_id
 * @param int $size
 * @param array $attr
 *
 * @return string
 */

function aazeen_filter_post_thumbnail_html( $html, $pid, $post_thumbnail_id, $size, $attr ) {

     if ( ! empty( $attr[ 'link_thumbnail' ] ) ) {

        $html = sprintf(
            '<a class="img-link" href="%s" title="%s" rel="nofollow"><span class="thumbnail-resize" >%s</span></a>',
            esc_url(get_permalink( $pid )),
            esc_attr( get_the_title( $pid ) ),
            $html
        );
     }
    return $html;
}

add_filter( 'post_thumbnail_html', 'aazeen_filter_post_thumbnail_html', 10, 5 );


/**
* comments meta
*/
if (! function_exists('aazeen_meta_comment')) :
function aazeen_meta_comment()
{
    if (! post_password_required() && (comments_open() || get_comments_number())) {
        echo '<span class="comments-link">';
        /* translators: %s: post title */
        comments_popup_link(sprintf(wp_kses(__('Leave a Comment<span class="screen-reader-text"> on %s</span>', 'aazeen'), array( 'span' => array( 'class' => array() ) )), get_the_title()));
        echo '</span>';
    }
}
endif;



/**
* Prints categories list.
*/
if (! function_exists('aazeen_category_list')) :
function aazeen_category_list()
{
    $categories = get_the_category();

    $output = '';
    if (is_single()) {
        $separator = '';
        if (! empty($categories)) {
            foreach ($categories as $category) {
                $output .=
                '<a class="label transparent " href="' . esc_url(get_category_link($category->term_id)) .
                '" alt="' . esc_attr(sprintf(__('View all posts in %s', 'aazeen'), $category->name)) . '">' . esc_html($category->name) . '</a>' . $separator;
            }
        }
    } else {
        $separator = '';
        if (! empty($categories)) {
            foreach ($categories as $category) {
                $output .=
                '<a class="label radius " href="' . esc_url(get_category_link($category->term_id)) .
                '" alt="' . esc_attr(sprintf(__('View all posts in %s', 'aazeen'), $category->name)) . '">' . esc_html($category->name) . '</a>' . $separator;
            }
        }
    }
    echo trim($output, $separator);
}
endif;



// Print categories for widgets
if (! function_exists('aazeen_category_widgtesmission')) :
function aazeen_category_widgtesmission()
{
    $categories = get_the_category();

    $output = '';

        $separator = '';
        if (! empty($categories)) {
            foreach ($categories as $category) {
                $output .=
                '<a class="label primary" href="' . esc_url(get_category_link($category->term_id)) .
                '" alt="' . esc_attr(sprintf(__('View all posts in %s', 'aazeen'), $category->name)) . '">' . esc_html($category->name) . '</a>' . $separator;
            }
        }
    echo trim($output, $separator);
}
endif;


if (! function_exists('aazeen_meta_tag')) :
/**
* Prints HTML with meta information for the tags .
*/
function aazeen_meta_tag()
{

// Hide category and tag text for pages.
    if ('post' === get_post_type()) {
        /* translators: used between list items, there is a space after the comma */
        $tags_list = get_the_tag_list('<sapn class="button-group"><button class="hollow button secondary radius">', '</button><button class="hollow button secondary radius">', '</button></span>');
        if ($tags_list) {
            echo '<span class="single-tag-text">';
            _e('Tagged:', 'aazeen');
            echo '</span>';
            echo $tags_list;
        }
    }
}
endif;

if (! function_exists('aazeen_edit_link')) :
/**
* Prints HTML with meta information for the tags .
*/
function aazeen_edit_link()
{
    edit_post_link(
      sprintf(
        /* translators: %s: Name of current post */
        esc_html__('Edit %s', 'aazeen'),
        the_title('<span class="screen-reader-text">"', '"</span>', false)
      ),
      '<button class="hollow button secondary" >',
      '</button>'
    );
}
endif;



if (! function_exists('aazeen_time_link')) :
/**
* Gets a nicely formatted string for the published date.
*/
function aazeen_time_link()
{
    $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';

    $time_string = sprintf(
      $time_string,
      get_the_date(DATE_W3C),
      get_the_date(),
      get_the_modified_date(DATE_W3C),
      get_the_modified_date()
    );
    $archive_year  = get_the_time('Y');
    $archive_month = get_the_time('m');


    // Wrap the time string in a link, and preface it with 'Posted on'.
    return sprintf(
      /* translators: %s: post date */
      __('<span class="screen-reader-text">Posted on</span> %s', 'aazeen'),
      '<span class="meta-info meta-info-date is-font-size-6"> <a href="' . esc_url(get_month_link($archive_year, $archive_month)) . '" rel="bookmark">' . $time_string . '</a></span>'
    );
}
endif;

/* Function which displays your post date in time ago format */
function aazeen_time_ago() {
	return human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ).' '.esc_html__( 'ago','aazeen' );
}

if (! function_exists('aazeen_author_bio')) :

function aazeen_author_bio()
{
    // Post meta author
    $author = sprintf(
      esc_html_x('Posts by: %s', 'post author', 'aazeen'),
      esc_html(get_the_author())
    );
    echo  $author ;
}
endif;

function aazeen_archive_title( $title ) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    } elseif ( is_tag() ) {
        $title = single_tag_title( '', false );
    } elseif ( is_author() ) {
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif ( is_post_type_archive() ) {
        $title = post_type_archive_title( '', false );
    } elseif ( is_tax() ) {
        $title = single_term_title( '', false );
    }

    return $title;
}

add_filter( 'get_the_archive_title', 'aazeen_archive_title' );
/**
* Post page title “Category:”, “Tag:”, “Author:”, “Archives:” and “Other taxonomy name:”
*/
if (! function_exists('aazeen_getpost_page_title')) :
function aazeen_getpost_page_title()
{
    if (is_category()) {
        $title = single_cat_title('', false);
    } elseif (is_tag()) {
        $title = single_tag_title('', false);
    } elseif (is_author()) {
        $title = aazeen_author_bio();
    } elseif (is_archive()) {
        $title = get_the_archive_title();
    } elseif (is_tax()) {
        $title = single_term_title('', false);
    } elseif (is_search()) {
        $title  = sprintf(esc_html__('Search Results for: %s', 'aazeen'), esc_html(get_search_query()));
    }
    return $title;
}
endif;

/**
* Post page main title “Category:”, “Tag:”, “Author:”, “Archives:” and “Other taxonomy name:”
*/
if (! function_exists('aazeen_mainpost_page_title')) :
function aazeen_mainpost_page_title()
{
    echo '<div id="sub_banner" >';
    echo '<div class="top-bar">';
    echo '<div class="top-bar-left">';
    echo '<div class="top-bar-title">';
    echo '<h2 class="subheader">';
    echo aazeen_getpost_page_title();
    echo '</h2>';
    echo '</div>';
    echo '</div>';
    echo '<div class="top-bar-right">';
    echo '<div class="breadcrumb-wrap">';
    echo aazeen_breadcrumb();
    echo '</div>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
}
endif;

if (! function_exists('aazeen_mainpost_blog_title')) :
function aazeen_mainpost_blog_title()
{
    echo '<div id="sub_banner">';
    echo '<div class="top-bar">';
    echo '<div class="top-bar-title blogtitle ">';
    echo '<h1 class="subheader">';
    echo esc_html__('Blog', 'aazeen');
    echo '</h1>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
}
endif;


/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function aazeen_body_classes( $classes ) {
	$select_header_style = get_theme_mod('select_header_style','header1');
  if ( 'header3' == $select_header_style) :
	/* Adds a class whether layout is boxed or wide */
	$classes[] = 'tranparenth3';
else:
  	$classes[] = 'no-tranparent';
endif;
	return $classes;
}
add_filter( 'body_class', 'aazeen_body_classes' );
