<?php
/**
 * aazeen Theme Custom color
 *
 * @package themezwp
 * @subpackage aazeen
 * @since aazeen 1.0.0
 */

function aazeen_inline_style() {
	$inline_css='';
	/*=============================================>>>>>
	= Color calculation for menu =
	===============================================>>>>>*/
	$header_stickbgtrans_color   =  get_theme_mod( 'header_stickbgtrans_color' ,'#fff') ;

	if ( 225 > ariColor::newColor( $header_stickbgtrans_color )->luminance ) {
		// Our background color is dark, so we need to create a light text color.
		$text_color_menustk = Kirki_Color::adjust_brightness( $header_stickbgtrans_color, 225 );
	} else {

		// Our background color is light, so we need to create a dark text color
		$text_color_menustk = Kirki_Color::adjust_brightness( $header_stickbgtrans_color, -225 );

	}
	$text_color_menustk = esc_attr( $text_color_menustk );
	/*  Color calculation for text */
	$inline_css .=
		".sticky_menu.active .main-menu-wrap .dropdown.menu a,
		.sticky_menu.header3 .is-dropdown-submenu > li a,
		.sticky_menu.active.header3,
		.sticky_menu.active .menu-outer .menu-icon::after,
		.sticky_menu.active .main-menu-wrap .dropdown.menu a,
		.sticky_menu.active .navbar-search .navbar-search-button .fa,
		.sticky_menu.active .offcanvas-trigger
		{
			color: $text_color_menustk;
		}"
	;
	/*=============================================>>>>>
	= Color calculation for Mobile menu text =
	===============================================>>>>>*/
	$mobheader_menubg_color   =  get_theme_mod( 'mobheader_menubg_color' ,'#fff') ;

	if ( 225 > ariColor::newColor( $mobheader_menubg_color )->luminance ) {
		// Our background color is dark, so we need to create a light text color.
		$text_color_menumobile = Kirki_Color::adjust_brightness( $mobheader_menubg_color, 225 );
	} else {

		// Our background color is light, so we need to create a dark text color
		$text_color_menumobile = Kirki_Color::adjust_brightness( $mobheader_menubg_color, -225 );

	}
	$text_color_menumobile = esc_attr( $text_color_menumobile );
	/*  Color calculation for text */
	$inline_css .=
		".off-canvas-wrapper .accordion-menu a
		{
			color: $text_color_menumobile;
		}";

		$inline_css .=
			".off-canvas-wrapper .accordion-menu .is-accordion-submenu-parent:not(.has-submenu-toggle)>a::after

			{
			border-top-color: $text_color_menumobile;
			}";


	/*=============================================>>>>>
	= Color calculation for hover color =
	===============================================>>>>>*/

$aazeen_hover_color   =  get_theme_mod( 'aazeen_hover_color' ,'#2f4052') ;


if ( 225 > ariColor::newColor( $aazeen_hover_color )->luminance ) {
	// Our background color is dark, so we need to create a light text color.
	$text_color_hover = Kirki_Color::adjust_brightness( $aazeen_hover_color, 225 );
} else {

	// Our background color is light, so we need to create a dark text color
	$text_color_hover = Kirki_Color::adjust_brightness( $aazeen_hover_color, -225 );

}
$text_color_hover = esc_attr( $text_color_hover );
/*  Color calculation for text */
$inline_css .=
	".dropdown .is-dropdown-submenu a:hover,
	.main-menu-wrap .dropdown.menu .is-dropdown-submenu a:hover,
	.btn.btn-primary:hover,
	.label:hover,
	.button.secondary:hover,
	.woocommerce div.product form.cart .button:hover,
	.woocommerce #respond input#submit.alt:hover,
	.woocommerce a.button.alt:hover,
	.woocommerce button.button.alt:hover,
	.woocommerce input.button.alt:hover,
	.woocommerce #respond input#submit:hover,
	.woocommerce a.button:hover,
	.woocommerce button.button:hover,
	.woocommerce input.button:hover,
.is-dropdown-submenu > li:hover

	{
		color: $text_color_hover !important;
	}"
;

// shadow color
$inline_css .=
	".btn.btn-primary:hover,
	.search-submit:hover
	{
		box-shadow:0 14px 26px -12px " . Kirki_Color::get_rgba($aazeen_hover_color, .42) . ", 0 4px 23px 0 rgba(0, 0, 0, .12), 0 8px 10px -5px rgba(156, 39, 176, .2);

	}"
;

$inline_css .=
	".feature .btn.btn-primary:hover
	{
		box-shadow:0 14px 26px -12px " . Kirki_Color::get_rgba($aazeen_hover_color, .42) . ", 0 4px 23px 0 rgba(0, 0, 0, .12), 0 8px 10px -5px rgba(156, 39, 176, .2) !important;
		color: $text_color_hover !important;
	}"
;
/*=============================================>>>>>
= Color calculation for text color flavor_color =
===============================================>>>>>*/
$aazeen_flavor_color   =  get_theme_mod( 'aazeen_flavor_color' ,'#1abc9c') ;


if ( 225 > ariColor::newColor( $aazeen_flavor_color )->luminance ) {
	// Our background color is dark, so we need to create a light text color.
	$text_color = Kirki_Color::adjust_brightness( $aazeen_flavor_color, 225 );
} else {

	// Our background color is light, so we need to create a dark text color
	$text_color = Kirki_Color::adjust_brightness( $aazeen_flavor_color, -225 );

}
$text_color = esc_attr( $text_color );
/*  Color calculation for text */
$inline_css .=
	".btn.btn-primary,
	a.btn.btn-primary,
	.search-submit,
	.label,
	.button.secondary,
	.woocommerce span.onsale,
	.woocommerce div.product form.cart .button,
	.woocommerce #respond input#submit.alt,
	.woocommerce a.button.alt,
	.woocommerce button.button.alt,
	.woocommerce input.button.alt,
	.woocommerce #respond input#submit,
	.woocommerce a.button,
	.woocommerce button.button,
	.woocommerce input.button,
	.card.card-contact .wpcf7-submit,
	.scroll_to_top.floating-action.button
	{
		color: $text_color;
	}"
;
// shadow color
$inline_css .=
	".btn.btn-primary,
	.card.card-contact .wpcf7-submit,
	.search-submit
	{
		box-shadow:0 2px 2px 0 " . Kirki_Color::get_rgba($aazeen_flavor_color, .14) . ",0 3px 1px -2px " . Kirki_Color::get_rgba($aazeen_flavor_color, .2) . ",0 1px 5px 0 " . Kirki_Color::get_rgba($aazeen_flavor_color, .12) . ";

	}"
;





/*=============================================>>>>>
= Color calculation for Footer widgets text =
===============================================>>>>>*/
$aazeen_widgets_bgcolor   =  get_theme_mod( 'aazeen_widgets_bgcolor' ,'#1a1c27') ;

if ( 225 > ariColor::newColor( $aazeen_widgets_bgcolor )->luminance ) {
	// Our background color is dark, so we need to create a light text color.
	$text_color_foowidgetbg = Kirki_Color::adjust_brightness( $aazeen_widgets_bgcolor, 225 );
} else {

	// Our background color is light, so we need to create a dark text color
	$text_color_foowidgetbg = Kirki_Color::adjust_brightness( $aazeen_widgets_bgcolor, -225 );

}
$text_color_foowidgetbg = esc_attr( $text_color_foowidgetbg );
/*  Color calculation for text */
$inline_css .=
	".top-footer-wrap .widget-title h3,
	.top-footer-wrap .widget_wrap ul li a,
	.top-footer-wrap .tag-cloud-link
	{
		color: $text_color_foowidgetbg;
	}";

	$inline_css .=
		".off-canvas-wrapper .accordion-menu .is-accordion-submenu-parent:not(.has-submenu-toggle)>a::after

		{
		border-top-color: $text_color_foowidgetbg;
		}";

if ( is_plugin_active( 'meta-box/meta-box.php' ) ) {

/*=============================================>>>>>
		= Meta box page head 3 =
===============================================>>>>>*/
$page_menu_textcolor = rwmb_meta( 'page_menu_textcolor');
/*  Color calculation for text */
if ( !empty ($page_menu_textcolor) ) :
$inline_css .=
".page.tranparenth3 .main-menu-wrap .dropdown.menu a,
.page.tranparenth3 .header3, .page.tranparenth3 .menu-outer .menu-icon::after,
.page.tranparenth3 .main-menu-wrap .dropdown.menu a,
.page.tranparenth3 .navbar-search .navbar-search-button .fa
{
	color: $page_menu_textcolor ;
}
.page.tranparenth3 .dropdown.menu > li.is-dropdown-submenu-parent > a::after
{
	border-top-color:$page_menu_textcolor;
}
";
endif;
$page_menu_bgcolor = rwmb_meta( 'page_menu_bgcolor');

$inline_css .=
".page.tranparenth3 .header3,
.page.tranparenth3 .header3 .is-dropdown-submenu > li,
.page.tranparenth3 .header3 .is-dropdown-submenu
{
	background: $page_menu_bgcolor ;
}"
;



$value_transparent_menu= rwmb_meta( 'enable_transparent_menu' );

if ( $value_transparent_menu ) :
$inline_css .=
" .page.tranparenth3 .header3.sticky
{
	position: relative;
}
.page.tranparenth3 .header3.sticky.active{
	position: fixed;
}
.page.tranparenth3 #sub_banner_page .heade-page-nothumb,
.page.tranparenth3 #sub_banner_page .single-page-thumb-outer .page-thumb
{
	padding-top:0px !important;
}
.page.tranparenth3 .header3,
.page.tranparenth3 .header3 .is-dropdown-submenu > li,
.page.tranparenth3 .header3 .is-dropdown-submenu
{
	background: $page_menu_bgcolor ;
}"
;
else:
	$inline_css .=
	".page.tranparenth3 .header3.sticky
	{
		position: fixed;
	}"
	;
endif;


/*=============================================>>>>>
= Meta box page =
===============================================>>>>>*/
$value_text_color = '#fff';
$value_text_color = rwmb_meta( 'page_sunheader_textcolor');
/*  Color calculation for text */
$inline_css .=
"#sub_banner_page h2
{
	color: $value_text_color ;
}"
;



$value_fontsizetitle = rwmb_meta( 'subheader_title_fontsize' );
$inline_css .=
	"@media print, screen and (min-width: 40em) {
		#sub_banner_page h2
	{
		font-size: ".$value_fontsizetitle."px ;
	}}"
;

/*=============================================>>>>>
= Meta box Post =
===============================================>>>>>*/

$post_sunheader_textcolor = rwmb_meta( 'post_sunheader_textcolor');
/*  Color calculation for text */
$inline_css .=
"#sub_single_page .single-headwarp h1,
#sub_single_page .single-headwarp .label a,
#sub_single_page .single-headwarp i
{
	color: $post_sunheader_textcolor ;
}"
;



$value_fontsizetitle = rwmb_meta( 'subheader_title_fontsize' );
$inline_css .=
	"@media print, screen and (min-width: 40em) {
		#sub_single_page .single-headwarp h1
	{
		font-size: ".$value_fontsizetitle."px ;
	}}"
;
} // end meta box
/*=============================================>>>>>
= Color calculation for Subheader text color =
===============================================>>>>>*/
$aazeen_subheader_bgcolor   =  get_theme_mod( 'aazeen_subheader_bgcolor' ,'#fff') ;


if ( 225 > ariColor::newColor( $aazeen_subheader_bgcolor )->luminance ) {
	// Our background color is dark, so we need to create a light text color.
	$text_color_subheader = Kirki_Color::adjust_brightness( $aazeen_subheader_bgcolor, 225 );
} else {

	// Our background color is light, so we need to create a dark text color
	$text_color_subheader = Kirki_Color::adjust_brightness( $aazeen_subheader_bgcolor, -225 );

}
$text_color_subheader = esc_attr( $text_color_subheader );
/*  Color calculation for text */
$inline_css .=
	"#sub_banner h1,
	#sub_banner .top-bar .breadcrumbs li,
	.woocommerce .woocommerce-breadcrumb a,
	.archive.tranparenth3 .header-wrap .header3 .main-menu-wrap .dropdown.menu a
	{
		color: $text_color_subheader;
	}"
;

wp_add_inline_style( 'aazeen-style', $inline_css );
}
add_action( 'wp_enqueue_scripts', 'aazeen_inline_style', 10 );
