<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 */
?>

	<!DOCTYPE html>
	<html <?php language_attributes();?> >

	<head>
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset');?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' );?>" />
		<?php endif; ?>
		<?php wp_head();?>
	</head>

	<body <?php body_class();?> >
		<?php /* start site warp */?>
		<?php $site_layout = get_theme_mod( 'site_layout', 'full' ); ?>
		<div id="site-wrapper" data-magellan class=" site_layout <?php  echo esc_attr($site_layout); ?> grid-container ">
			<div class="header-wrap">
				<?php
				$select_header_style = get_theme_mod('select_header_style','header1');
				get_template_part('template-parts/header/part', ''.$select_header_style .'');?>

				<?php get_template_part( 'template-parts/menu/header', 'mobile' ); ?>
			</div>
			<div id="content" class="site-mask ">
