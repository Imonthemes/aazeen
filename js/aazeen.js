(function($) {
    'use strict';

    function PagePreloader() {
      jQuery('body').removeClass('no-js');
    }
    jQuery(window).load(function($) {
      PagePreloader();
    });
  }

)(window.jQuery);
/* --------------------------------------------
JS Start
-------------------------------------------- */

jQuery(document).ready(function($) {
  /* call foundation */
  $(document).foundation();

// sticky menu animation
  $('.sticky_menu.sticky').on('sticky.zf.stuckto:top', function() {
    $(this).addClass(' fadeInDown');
  }).on('sticky.zf.unstuckfrom:top', function() {
    $(this).removeClass(' fadeInDown');
  })
  // carousel post

  $('.carousel-layout4').slick({
    cssEase: 'linear',
    pauseOnHover: false,
    prevArrow: '<div  class="aazeen-slider-nav aazeen-slider-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
    nextArrow: '<div  class="aazeen-slider-nav aazeen-slider-next "><i class="fa fa-angle-right" aria-hidden="true"></i></div>',
    responsive: [{
      breakpoint: 768,
      settings: {
        adaptiveHeight: true,
        slidesToShow: 1,
        centerMode: false,
        centerPadding: '0px',
      }
    }, ]
  });



  // scrollup
  jQuery(window).bind("scroll", function() {
    if (jQuery(this).scrollTop() > 800) {
      jQuery(".scroll_to_top").fadeIn('slow');
    } else {
      jQuery(".scroll_to_top").fadeOut('fast');
    }
  });
  jQuery(".scroll_to_top").click(function() {
    jQuery("html, body").animate({
      scrollTop: 0
    }, "slow");
    return false;
  });

// sticky header
  $(window).on("scroll", function() {
      if($(window).scrollTop() > 100) {
          $(".header3.sticky").addClass("active");
      } else {
          //remove the background property so it comes transparent again (defined in your css)
         $(".header3.sticky").removeClass("active");
      }
  });

if (Foundation.MediaQuery.atLeast('medium')) {
// Adjust height of header 3
     var divHeight = $('.header3').outerHeight();
     $('.page-thumb').css('padding-top', divHeight+'px');
     var divHeightcontent = $('.header3').outerHeight()+30;
     $('#content-page.no-page-header').css('padding-top', divHeightcontent+'px');

     var divHeightsub = $('.header3').outerHeight();
     $('#sub_banner').css('padding-top', divHeightsub+'px');

     var divHeightblog = $('.header3').outerHeight();
     $('.home.blog #blog-content').css('padding-top', divHeightblog+'px');

}
if (Foundation.MediaQuery.atLeast('medium')) {
  var big_image = $( '.header-filter[data-parallax="active"]' );
  if ( big_image.length !== 0 ) {
    $( window ).on(
      'scroll', debounce(
        function () {
            var oVal = ($( window ).scrollTop() / 3);
            big_image.css(
              {
                'transform': 'translate3d(0,' + oVal + 'px,0)',
                '-webkit-transform': 'translate3d(0,' + oVal + 'px,0)',
                '-ms-transform': 'translate3d(0,' + oVal + 'px,0)',
                '-o-transform': 'translate3d(0,' + oVal + 'px,0)'
              }
            );
        }, 4
      )
    );
  }
}
function debounce( func, wait, immediate ) {
  var timeout;
  return function () {
    var context = this,
      args = arguments;
    clearTimeout( timeout );
    timeout = setTimeout(
      function () {
        timeout = null;
        if ( !immediate ) {
          func.apply( context, args );
        }
      }, wait
    );
    if ( immediate && !timeout ) {
      func.apply( context, args );
    }
  };
}
jQuery(".slider").not('.slick-initialized').slick();

}); // js end

// Ripple Effect
// by Craig Tuttle
// http://codepen.io/Craigtut/pen/dIfzv

(function (window, $) {

  $(function() {


    $('.ripple, .button, .tabs-title,.btn, .wpcf7-submit').on('click', function (event) {

      var $div = $('<div/>'),
          btnOffset = $(this).offset(),
      		xPos = event.pageX - btnOffset.left,
      		yPos = event.pageY - btnOffset.top;



      $div.addClass('ripple-effect');
      var $ripple = $(".ripple-effect");

      $ripple.css("height", $(this).height());
      $ripple.css("width", $(this).height());
      $div
        .css({
          top: yPos - ($ripple.height()/2),
          left: xPos - ($ripple.width()/2),
          background: $(this).data("ripple-color")
        })
        .appendTo($(this));

      window.setTimeout(function(){
        $div.remove();
      }, 2000);
    });

  });

})(window, jQuery);


( function( $ ) {
	'use strict';

  /* Flexslider ---------------------*/
	function slickSliderSetup() {


// Slider styl 1 animation
  $(".modern-Slider").not('.slick-initialized').slick({
    autoplaySpeed:10000,
    speed:600,
    slidesToShow:1,
    slidesToScroll:1,
    pauseOnHover:false,
    dots:true,
    pauseOnDotsHover:true,
    cssEase:'linear',
   // fade:true,
    draggable:false,
    prevArrow: '<div  class="aazeen-slider-nav aazeen-slider-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
    nextArrow: '<div  class="aazeen-slider-nav aazeen-slider-next "><i class="fa fa-angle-right" aria-hidden="true"></i></div>',
  });
  // Slider style two
    $('.slider2').not('.slick-initialized').slick({
      centerMode: true,
 centerPadding: '160px',
      slidesToShow:1,
      cssEase: 'linear',
      pauseOnHover:false,
      prevArrow: '<div  class="aazeen-slider-nav aazeen-slider-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
      nextArrow: '<div  class="aazeen-slider-nav aazeen-slider-next "><i class="fa fa-angle-right" aria-hidden="true"></i></div>',
      responsive: [
    {
      breakpoint: 768,
      settings: {
        adaptiveHeight: true,
        slidesToShow:1,
        centerMode: false,
        centerPadding: '0px',
      }
    },
  ]
    });

  }

  $( window ).load( slickSliderSetup );
	$( document ).ajaxComplete( slickSliderSetup );

})( jQuery );

/* --------------------------------------------
JS END
-------------------------------------------- */
