<?php
/**
 * Add sections
 */


/* adding Header Options section*/
Kirki::add_section( 'aazeen_upgradepro_options', array(
    'title'          =>esc_attr__( 'About Theme Info ', 'aazeen' ),
     'panel'          => 'upgradepro_options', // Not typically needed.
    'priority'       => 1,
    'type'           => 'expanded',
    'capability'     => 'edit_theme_options',
) );

Kirki::add_section( 'aazeen_appearance_options', array(
    'title'          =>esc_attr__( 'Site Appearance', 'aazeen' ),
     'panel'          => 'aazeen_theme_options', // Not typically needed.
    'priority'       => 1,
    'icon' => 'dashicons-admin-customizer'
) );

Kirki::add_section( 'aazeen_header_options', array(
    'title'          =>esc_attr__( 'Header Options', 'aazeen' ),
     'panel'          => 'aazeen_theme_options', // Not typically needed.
    'priority'       => 1,
    'icon' => 'dashicons-menu'
) );

Kirki::add_section( 'aazeen_subheader_options', array(
    'title'          =>esc_attr__( 'Sub Header Options', 'aazeen' ),
     'panel'          => 'aazeen_theme_options', // Not typically needed.
    'priority'       => 1,
    'icon' => 'dashicons-menu'
) );

Kirki::add_section( 'aazeen_postlayout_settings', array(
    'title'          =>esc_attr__( 'Post layout Options ', 'aazeen' ),
     'panel'          => 'aazeen_theme_options', // Not typically needed.
    'priority'       => 3,
      'icon' => 'dashicons-layout'
) );

Kirki::add_section( 'aazeen_copyright_settings', array(
    'title'          =>esc_attr__( 'Footer Options ', 'aazeen' ),
     'panel'          => 'aazeen_theme_options', // Not typically needed.
    'priority'       => 3,
    'icon' => 'dashicons-feedback'
) );
