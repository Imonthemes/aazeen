<?php

/**
 * Add panels
 */


/* adding laaazeent panel */
Kirki::add_panel( 'upgradepro_options', array(
    'priority'    => 10,
    'title'       => esc_attr__( 'About Theme', 'kirki' ),
    'description' => esc_attr__( 'This panel will provide all Site layout and Background color typography options of the Theme.', 'kirki' ),
    'icon' => 'dashicons-warning'
) );


Kirki::add_panel( 'aazeen_theme_options', array(
    'priority'    => 10,
    'title'       => esc_attr__( 'Theme options', 'aazeen' ),
    'description' => esc_attr__( 'This panel will provide all Site layout and Background color typography options of the Theme.', 'aazeen' ),
    'icon' => 'dashicons-admin-generic'
) );

Kirki::add_panel( 'aazeen_home_options', array(
    'priority'    => 10,
    'title'       => esc_attr__( 'Home sections', 'aazeen' ),
    'description' => esc_attr__( 'This panel will provide home page sections options', 'aazeen' ),
    'icon' => 'dashicons-admin-generic'
) );
