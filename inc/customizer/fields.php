<?php
Kirki::add_config( 'aazeen_option', array(
	'capability'  => 'edit_theme_options',
	'option_type' => 'theme_mod',
) );

$socialarray = array(
		'' => esc_attr__('Please Select', 'aazeen'),
		'facebook' =>esc_attr__('Facebook', 'aazeen'),
		'dribbble' => esc_attr__('Dribbble', 'aazeen'),
		'twitter' => esc_attr__('Twitter', 'aazeen'),
		'google' => esc_attr__('google plus', 'aazeen'),
		'skype' => esc_attr__('skype', 'aazeen'),
		'youtube' => esc_attr__('Youtube', 'aazeen'),
		'flickr' => esc_attr__('Flickr', 'aazeen'),
		'pinterest' => esc_attr__('Pinterest', 'aazeen'),
		'vk' => esc_attr__('vk', 'aazeen'),
		'rss' => esc_attr__('RSS', 'aazeen'),
		'tumblr' => esc_attr__('Tumblr', 'aazeen'),
		'instagram' => esc_attr__('Instagram', 'aazeen'),
		'xing' => esc_attr__('Xing', 'aazeen')
);
//**** aazeen upsell pro */
Kirki::add_field( 'aazeen_option', array(
	'type'        => 'custom',
	'settings'    => 'aazeen_option_view_link_pro',
	'section'     => 'aazeen_upgradepro_options',
	'default'     => '<a class="button-error  button-upsell" target="_blank" href="' . esc_url( 'https://www.themezwp.com/aazeen-wordpress-theme/#panel-3392-13-0-1' ) . '">'.esc_html__( 'Upgrade To Pro', 'aazeen' ).'</a>',
	'priority'    => 10,
) );


Kirki::add_field( 'aazeen_option', array(
	'type'        => 'custom',
	'settings'    => 'aazeen_option_view_link2',
	'section'     => 'aazeen_upgradepro_options',
	'default'     => '<a class="button-blue  button-upsell" target="_blank" href="' . esc_url( 'https://wordpress.org/support/theme/aazeen' ) . '">'.esc_html__( 'Support', 'aazeen' ).'</a>',
	'priority'    => 30,
) );


Kirki::add_field( 'aazeen_option', array(
	'type'        => 'custom',
	'settings'    => 'aazeen_option_view_link3',
	'section'     => 'aazeen_upgradepro_options',
	'default'     => '<a class="button-warning  button-upsell" target="_blank" href="' . esc_url( 'http://themezwp.com/aazeen-demo/documentation-usage/' ) . '">'.esc_html__( 'Read the documentation', 'aazeen' ).'</a>',
	'priority'    => 50,
) );

Kirki::add_field( 'aazeen_option', array(
	'type'        => 'checkbox',
	'settings'    => 'on_of_frontpage_content',
	'label'       => esc_attr__( 'Show selected page content ', 'aazeen' ),
	'description' => esc_attr__( 'Show only Page content and disable front page section  ', 'aazeen' ),
	'section'     => 'static_front_page',
	'default'     => false,
) );


Kirki::add_field( 'aazeen_option', array(
	'type'        => 'select',
	'settings'    => 'site_layout',
	'label'       => __( 'select site layout', 'aazeen' ),
	'section'     => 'aazeen_appearance_options',
	'default'     => 'full',
	'priority'    => 10,
	'transport' => 'postMessage',
	'choices'     => array(
		'box_wbb z-depth-2' => esc_attr__( 'Box Layout', 'aazeen' ),
		'full' => esc_attr__( 'Full Layout', 'aazeen' ),
	),
) );

Kirki::add_field('aazeen_option', array(
    'type' => 'color',
    'settings' => 'aazeen_flavor_color',
    'label' => esc_attr__('Primary Color', 'aazeen'),
    'section' => 'aazeen_appearance_options',
    'default' => '#1abc9c',
    'transport' => 'auto',
    'priority' => 10,
    'choices' => array(
        'alpha' => true
    ),
    'output' => array(
        array(
            'element' => '.woocommerce div.product p.price, .woocommerce div.product span.price,.single-product .product_meta span > *,.post-wrap-layout-2 .card .category.text-info a,.button.hollow.secondary,.single-header-warp .post-meta a,.comment-title h2,h2.comment-reply-title,.logged-in-as a,.author-title a,.woocommerce ul.products li.product a, .woocommerce ul.products li.product .woocommerce-loop-category__title, .woocommerce ul.products li.product .woocommerce-loop-product__title, .woocommerce ul.products li.product h3, .woocommerce ul.products li.product .price, .woocommerce div.product .woocommerce-tabs ul.tabs li.active a,.woocommerce .star-rating span::before,.card .card-footer .right .btn.add_to_cart_button,.woocommerce div.product .woocommerce-tabs ul.tabs.wc-tabs li.active a,.woocommerce-product-rating a',
            'property' => 'color',
            'units' => ''
        ),
        array(
            'element' => '.search-submit,#burger span,.modern-Slider .slick-dots li,.card.card-contact .wpcf7-submit,.label,.btn.btn-primary,.woocommerce span.onsale,.comment-list .comment-reply-link,.navigation .nav-links .current,.single-cats.button-group .button,.aazeen-author-bttom .button,.comment-form .form-submit input#submit, a.box-comment-btn, .comment-form .form-submit input[type="submit"],.scroll_to_top.floating-action.button,.button.secondary,.block-content-none .search-submit,h1.entry-title::after,.woocommerce div.product form.cart .button, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button',
						'property' => 'background',
            'units' => ''
        ),
				array(
						'element' => '.sidebar-inner .widget_wrap,.navbar-search-bar-container,.multilevel-offcanvas.off-canvas.is-transition-overlap.is-open,.button.hollow.secondary,.single-header-warp,.woocommerce div.product .woocommerce-tabs ul.tabs.wc-tabs li.active a',
						'property' => 'border-color',
						'units' => ''
				),
    )
));

Kirki::add_field('aazeen_option', array(
    'type' => 'color',
    'settings' => 'aazeen_hover_color',
    'label' => esc_attr__('Hover Color', 'aazeen'),
    'section' => 'aazeen_appearance_options',
    'default' => '#2f4052',
    'transport' => 'auto',
    'priority' => 10,
    'choices' => array(
        'alpha' => true
    ),
		'output' => array(
        array(
            'element' => '.card .card-footer .right .button.add_to_cart_button:hover',
            'property' => 'color',
            'units' => ''
        ),
        array(
            'element' => '.is-dropdown-submenu > li:hover,.feature a.btn.btn-primary:hover,.label:hover,.btn.btn-primary:hover,.header3 .top-bar .is-dropdown-submenu .is-dropdown-submenu-item:hover,.block-content-none .search-submit:hover,.button.secondary:not(.hollow):hover,.woocommerce div.product form.cart .button:hover,.woocommerce #respond input#submit.alt:hover,.woocommerce a.button.alt:hover,.woocommerce button.button.alt:hover,.woocommerce input.button.alt:hover,.woocommerce #respond input#submit:hover,.woocommerce a.button:hover,.woocommerce button.button:hover,.woocommerce input.button:hover',
						'property' => 'background',
            'units' => '!important'
        ),
				array(
						'element' => '.feature .btn.btn-primary:hover',
						'property' => 'background',
						'units' => '!important'
				),

				array(
						'element' => 'div.wpcf7-validation-errors, div.wpcf7-acceptance-missing,.button.hollow.secondary:hover,.woocommerce div.product .woocommerce-tabs ul.tabs.wc-tabs li.active a:hover,.woocommerce div.product .woocommerce-tabs ul.tabs.wc-tabs li a:hover',
						'property' => 'border-color',
						'units' => ''
				),
    )

));
/*=============================================>>>>>
= Header Options =
===============================================>>>>>*/
Kirki::add_field( 'aazeen_option', array(
	'type'        => 'select',
	'settings'    => 'select_header_style',
	'label'       => __( 'Header Style', 'aazeen' ),
	'section'     => 'aazeen_header_options',
	'default'     => 'header1',
	'multiple'    => 1,
	'choices'     => array(
		'header1' => esc_attr__( 'Style 1', 'aazeen' ),
		'header2' => esc_attr__( 'Style 2', 'aazeen' ),
		'header3' => esc_attr__( 'Style 3 (teransparent)', 'aazeen' )
	),
) );

Kirki::add_field( 'aazeen_option', array(
	'type'        => 'dimensions',
	'settings'    => 'header_main_padding',
	'label'       => __( 'Main Header height', 'aazeen' ),
	'section'     => 'aazeen_header_options',
	'transport' => 'auto',
	'default'     => array(
			'top'    => '5px',
			'bottom' => '5px',
			'left'   => '25px',
			'right'  => '25px',
		),
	'output' => array(
			array(
					'element' => '.header-wrap .banner-warp,.header-wrap .header1,.header-wrap .header3 ',
					'property' => 'padding',
			)
	),
) );


Kirki::add_field( 'aazeen_option', array(
	'type'        => 'color',
	'settings'    => 'header_solidbg_color',
	'label'       => __( 'background color(Header)', 'aazeen' ),
	'section'     => 'aazeen_header_options',
	'default'     => '#fff',
	'transport' => 'auto',
	'choices'     => array(
		'alpha' => true,
	),
	'output' => array(
			array(
					'element' => '.header1 .is-dropdown-submenu > li,.header1 .is-dropdown-submenu,.banner-warp,.header1',
					'property' => 'background',
					'units' => ''
			)
	),
	'active_callback' => array(
	array(
		'setting' => 'select_header_style',
		'operator' => '!==',
		'value' =>  'header3'
	)),

) );



Kirki::add_field( 'aazeen_option', array(
	'type'        => 'color',
	'settings'    => 'header_titledic_text',
	'label'       => __( 'Title And description color', 'aazeen' ),
	'section'     => 'aazeen_header_options',
	'default'     => '#0a0a0a',
	'transport' => 'auto',
	'choices'     => array(
		'alpha' => true,
	),
	'output' => array(
			array(
					'element' => '.header-wrap .banner-warp .site-branding p,.site-branding h1 a',
					'property' => 'color',
					'units' => ''
			)
	),

) );
// transparent header
Kirki::add_field( 'aazeen_option', array(
	'type'        => 'color',
	'settings'    => 'header_solidbgtrans_color',
	'label'       => __( 'background color(Header)', 'aazeen' ),
	'section'     => 'aazeen_header_options',
	'default'     => 'rgba(181,181,181,0.44)',
	'transport' => 'auto',
	'choices'     => array(
		'alpha' => true,
	),
	'output' => array(
			array(
					'element' => '.header3',
					'property' => 'background',
					'units' => ''
			)
	),
	'active_callback' => array(
	array(
		'setting' => 'select_header_style',
		'operator' => '==',
		'value' =>  'header3'
	)),
) );

Kirki::add_field( 'aazeen_option', array(
	'type'        => 'color',
	'settings'    => 'header_stickbgtrans_color',
	'label'       => __( 'background color(sticky Header)', 'aazeen' ),
	'section'     => 'aazeen_header_options',
	'default'     => '#fff',
	'transport' => 'auto',
	'choices'     => array(
		'alpha' => true,
	),
	'output' => array(
			array(
					'element' => '.tranparenth3 .header3.active,.tranparenth3 .header3 .is-dropdown-submenu > li,.header3 .is-dropdown-submenu,body.tranparenth3 .header3.active .is-dropdown-submenu,.page.tranparenth3 .header3.active .is-dropdown-submenu',
					'property' => 'background',
					'units' => ''
			)
	),
	'active_callback' => array(
	array(
		'setting' => 'select_header_style',
		'operator' => '==',
		'value' =>  'header3'
	)),

) );

Kirki::add_field('aazeen_option', array(
    'type' => 'custom',
    'settings' => 'themezwp_seperator_menustyle',
    'section' => 'aazeen_header_options',
    'default' => '<h2 class="themezwp-kirki-seperator">' . esc_html__('Menu Options', 'aazeen') . '</h2>'
));

Kirki::add_field( 'aazeen_option', array(
	'type'        => 'switch',
	'settings'    => 'sticky_menu_onof',
	'label'       => esc_attr__( 'Enable/Disable sticky Menu', 'aazeen' ),
	'section'     => 'aazeen_header_options',
	'default'     => '1',
	'priority'    => 10,
	'choices'     => array(
		'on'  => esc_attr__( 'Enable', 'aazeen' ),
		'off' => esc_attr__( 'Disable', 'aazeen' ),
	),
) );

Kirki::add_field( 'aazeen_option', array(
	'type'        => 'color',
	'settings'    => 'header_menubg2_color',
	'label'       => __( 'background color(Menu)', 'aazeen' ),
	'section'     => 'aazeen_header_options',
	'default'     => '#fff',
	'transport' => 'auto',
	'choices'     => array(
		'alpha' => true,
	),
	'output' => array(
			array(
					'element' => '.header-wrap .header2 .menu-outer,.header2 .menu-outer .is-dropdown-submenu',
					'property' => 'background',
					'units' => ''
			)
	),
	'active_callback' => array(
	array(
		'setting' => 'select_header_style',
		'operator' => '==',
		'value' =>  'header2'
	),
),

) );
Kirki::add_field( 'aazeen_option', array(
	'type'        => 'color',
	'settings'    => 'menu_text_color',
	'label'       => esc_attr__( 'Menu Text color', 'aazeen' ),
	'section'     => 'aazeen_header_options',
	'default'     => '#0a0a0a',
	'transport' => 'auto',
	'choices'     => array(
		'alpha' => true,
	),
	'output' => array(
			array(
					'element' => '.header3,.menu-outer .menu-icon::after,.main-menu-wrap .dropdown.menu a,.navbar-search .navbar-search-button .fa,.offcanvas-trigger ',
					'property' => 'color',
					'units' => ''
			),
			array(
					'element' => '.dropdown.menu > li.is-dropdown-submenu-parent > a::after',
					'property' => 'border-top-color',
					'units' => ''
			),
			array(
					'element' => '.is-dropdown-submenu .is-dropdown-submenu-parent.opens-left > a::after',
					'property' => 'border-right-color',
					'units' => ''
			)
	),

) );


Kirki::add_field('aazeen_option', array(
    'type' => 'repeater',
    'label' => esc_attr__('Add social icon', 'aazeen'),
    'section' => 'aazeen_header_options',
    'priority' => 10,
    'row_label' => array(
        'type' => 'field',
        'value' => esc_attr__('Social', 'aazeen'),
        'field' => 'social_icon'
    ),
    'settings' => 'social_icons_top',
    'fields' => array(
        'social_icon' => array(
            'type' => 'select',
            'label' => esc_attr__('Icon', 'aazeen'),
            'default' => '',
            'choices' =>$socialarray,
        ),
        'social_url' => array(
            'type' => 'url',
            'label' => esc_attr__('Link URL', 'aazeen'),
            'default' => ''
        ),
    )
));

Kirki::add_field('aazeen_option', array(
    'type' => 'custom',
    'settings' => 'themezwp_seperator_mobilemenustyle',
    'section' => 'aazeen_header_options',
    'default' => '<h2 class="themezwp-kirki-seperator">' . esc_html__('Mobile Menu Options', 'aazeen') . '</h2>'
));

Kirki::add_field( 'aazeen_option', array(
	'type'        => 'color',
	'settings'    => 'mobheader_menubg_color',
	'label'       => __( 'background color(Mobile Header)', 'aazeen' ),
	'section'     => 'aazeen_header_options',
	'default'     => '#fff',
	'transport' => 'auto',
	'choices'     => array(
		'alpha' => true,
	),
	'output' => array(
			array(
					'element' => '.header-wrap .mobile-header,.multilevel-offcanvas.off-canvas.is-transition-overlap.is-open',
					'property' => 'background',
					'units' => ''
			)
	),
) );
/*=============================================>>>>>
= Sub Header Options =
===============================================>>>>>*/
Kirki::add_field( 'aazeen_option', array(
	'type'        => 'switch',
	'settings'    => 'aazeen_subheader_onof',
	'label'       => esc_attr__( 'Enable/Disable subheader', 'aazeen' ),
	'section'     => 'aazeen_subheader_options',
	'default'     => '1',
	'priority'    => 10,
	'choices'     => array(
		'on'  => esc_attr__( 'Enable', 'aazeen' ),
		'off' => esc_attr__( 'Disable', 'aazeen' ),
	),
) );

Kirki::add_field('aazeen_option', array(
    'type' => 'color',
    'settings' => 'aazeen_subheader_bgcolor',
    'label' => esc_attr__('subheader background color', 'aazeen'),
    'section' => 'aazeen_subheader_options',
    'default' => '#fff',
    'transport' => 'auto',
    'priority' => 10,
    'choices' => array(
        'alpha' => true
    ),
    'output' => array(
        array(
            'element' => '#sub_banner',
            'property' => 'background',
            'units' => ''
        )
    )

));



/*=============================================>>>>>
= Footer Options =
===============================================>>>>>*/
Kirki::add_field('aazeen_option', array(
    'type' => 'color',
    'settings' => 'aazeen_widgets_bgcolorfull',
    'label' => esc_attr__('Full Widgets background color', 'aazeen'),
    'section' => 'aazeen_copyright_settings',
    'default' => '#1a1c27',
    'transport' => 'auto',
    'priority' => 10,
    'choices' => array(
        'alpha' => true
    ),
    'output' => array(
        array(
            'element' => '#footer .top-footer-wrap-full',
            'property' => 'background-color',
            'units' => ''
        )
    )

));

Kirki::add_field('aazeen_option', array(
    'type' => 'color',
    'settings' => 'aazeen_widgets_bgcolor',
    'label' => esc_attr__('Widgets background color', 'aazeen'),
    'section' => 'aazeen_copyright_settings',
    'default' => '#1a1c27',
    'transport' => 'auto',
    'priority' => 10,
    'choices' => array(
        'alpha' => true
    ),
    'output' => array(
        array(
            'element' => '#footer .top-footer-wrap',
            'property' => 'background-color',
            'units' => ''
        )
    )

));



/*----------- Footer COPYRIGHT options -----------*/

Kirki::add_field('aazeen_option', array(
    'type' => 'color',
    'settings' => 'aazeen_copyright_bgcolor',
    'label' => esc_attr__('Copyright background color', 'aazeen'),
    'section' => 'aazeen_copyright_settings',
    'default' => '#242424',
    'transport' => 'auto',
    'priority' => 10,
    'choices' => array(
        'alpha' => true
    ),
    'output' => array(
        array(
            'element' => '#footer .footer-copyright-wrap',
            'property' => 'background-color',
            'units' => ''
        )
    )

));

Kirki::add_field( 'aazeen_option', array(
	'type'        => 'typography',
	'settings'    => 'aazeen_copyright_typography',
	'label'       => esc_attr__( 'Copyright typography', 'aazeen' ),
	'section'     => 'aazeen_copyright_settings',
  'transport' => 'auto',
	'default'     => array(
		'font-family'    => 'Roboto',
		'variant'        => '400',
		'font-size'      => '16px',
		'line-height'    => '1.5',
		'letter-spacing' => '0',
		'subsets'        => array( 'latin-ext' ),
		'color'          => '#fff',
		'text-transform' => 'none',
		'text-align'     => 'center'
	),
	'priority'    => 10,
  'output' => array(
      array(
          'element' => '.copy-text,#footer .footer-copyright-wrap,.footer-copyright-text p,.footer-copyright-wrap a,.footer-copyright-wrap li,.footer-copyright-wrap ul,.footer-copyright-text ol',
          'property' => 'color',
          'units' => ''
      ),
  ),
	'choices' => array(
			'fonts' => array(
				'google' => array( 'popularity', 20 ),
			),
		),
) );

Kirki::add_field('aazeen_option', array(
    'type' => 'editor',
    'settings' => 'aazeen_footertext',
    'label' => __('Copyright text', 'aazeen'),
    'section' => 'aazeen_copyright_settings',
		'transport' => 'postMessage',
    'priority' => 10,
	));



/*=============================================>>>>>
= post layout options =
===============================================>>>>>*/

Kirki::add_field('aazeen_option', array(
    'type' => 'radio-image',
    'settings' => 'layout_page_gen',
    'label' => esc_html__('Post Layout', 'aazeen'),
    'section' => 'aazeen_postlayout_settings',
    'default' => 'content1',
    'priority' => 10,
    'choices' => array(
        'content1' => get_template_directory_uri() . '/images/list-layout-listing.svg',
        'content2' => get_template_directory_uri() . '/images/list-layout-grid.svg',
    )
));
Kirki::add_field( 'aazeen_option', array(
	'type'        => 'checkbox',
	'settings'    => 'checkbox_read_more',
	'label'       => esc_attr__( 'on/off readmore', 'aazeen' ),
	'section'     => 'aazeen_postlayout_settings',
	'default'     => false,
) );
Kirki::add_field('aazeen_option', array(
    'type' => 'radio-image',
    'settings' => 'sidbar_position_gen',
    'label' => esc_html__('Layout Sidebar', 'aazeen'),
    'section' => 'aazeen_postlayout_settings',
    'default' => 'right',
    'priority' => 10,
    'choices' => array(
        'full' => get_template_directory_uri() . '/images/fullwidth.svg',
        'left' => get_template_directory_uri() . '/images/left-sidebar.svg',
        'right' => get_template_directory_uri() . '/images/right-sidebar.svg',
    )
));
