<?php
/**
 * aazeen Theme Customizer
 *
 * @package aazeen
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function aazeen_customize_register($wp_customize)
{
    $wp_customize->get_setting('blogname')->transport         = 'postMessage';
    $wp_customize->get_setting('blogdescription')->transport  = 'postMessage';


/*----------- Move Customizer default Control -----------*/
$aazeen_appearance_options = $wp_customize->get_control('background_color');
if ($aazeen_appearance_options) {
    $aazeen_appearance_options->section = 'aazeen_appearance_options';
}

$homewidgetsarea_section = $wp_customize->get_section('sidebar-widgets-home-sidebar');
if (! empty($homewidgetsarea_section)) {
    $homewidgetsarea_section->panel = 'aazeen_home_options';
    $homewidgetsarea_section->title   = esc_attr__('Home Sections', 'aazeen');
    $homewidgetsarea_section->priority = 2;
    $homewidgetsarea_section->type = 'expanded';
}


}
add_action('customize_register', 'aazeen_customize_register');




/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function aazeen_customize_preview_jscss()
{
    wp_enqueue_style( 'aazeen-customizer-css', get_template_directory_uri() . '/css/customizer.min.css' );

    wp_enqueue_script('aazeen_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true);
}
add_action('customize_preview_init', 'aazeen_customize_preview_jscss');



require get_template_directory() . '/inc/customizer/config.php';
require get_template_directory() . '/inc/customizer/panels.php';
require get_template_directory() . '/inc/customizer/sections.php';
require get_template_directory() . '/inc/customizer/fields.php';
