<?php
/**
 * The front page template file.
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Aazeen
 * @since Aazeen 1.0
 */
get_header();

  if ( true == get_theme_mod( 'on_of_frontpage_content', false ) ) :
      include( get_page_template() );
  else:
    if ( is_active_sidebar( 'home-sidebar' ) ) :
      dynamic_sidebar( 'home-sidebar' );
    endif;
  endif;

get_footer();
