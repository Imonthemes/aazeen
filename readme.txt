=== Aazeen ===
Contributors: Themezwp
Requires at least: WordPress 4.7
Tested up to: WordPress 5.0-trunk
Version: 1.5.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: one-column, two-columns, right-sidebar, flexible-header, accessibility-ready, custom-colors, custom-header, custom-menu, custom-logo, editor-style, featured-images, footer-widgets, post-formats, rtl-language-support, sticky-post, theme-options, threaded-comments, translation-ready

== Description ==
Aazeen WordPress theme contains everything you need: professional design homepage, 10+ Preset homepage, 10+ Preset pages and you can import any page with single click .  This theme is compatible with a most popular plugin: SiteOrigin Page Builder.This Plugin is easy to use drag and drop page builder that will help you to create any layout you can imagine fast and easy. This theme offers wonderful features and functionalities to launch successful fundraising campaigns for individuals, companies, agencies, and different businesses. No coding skills required!

== Installation ==

1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Type in Aazeen in the search form and press the 'Enter' key on your keyboard.
3. Click on the 'Activate' button to use your new theme right away.
5. Navigate to Appearance > Customize in your admin panel and customize to taste.

== Copyright ==

Aazeen WordPress Theme, Copyright 2018 Themezwp
Aazeen is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

Aazeen bundles the following third-party resources:

License: CC0 licenses
Slider and screenshot image :
https://www.pexels.com/photo/green-iphone-5c-near-macbook-163143/

FontAwesome,
http://fontawesome.io/
Copyright 2015, Dave Gandy,
Font Awesome licensed under SIL OFL 1.1, http://scripts.sil.org/OFL
Code licensed under MIT License, http://www.opensource.org/licenses/MIT
Documentation licensed under CC BY 3.0, http://creativecommons.org/licenses/by/3.0/

slick.js
Author: Ken Wheeler
Website: http://kenwheeler.github.io
License:MIT License

Animate.CSS
http://daneden.me/animate
License:MIT License

framework Credits

Plugin Name:   Kirki Toolkit
Plugin URI:    http://aristath.github.io/kirki
License: MIT License

Foundation for Sites by ZURB
Foundation.css
Foundation.js
URI: https://foundation.zurb.com/
License:MIT License


== Changelog ==

Initial release
