<?php

/**
 * aazeen functions and definitions
 *
 * For more information on hooks, actions, and filters, @link http://codex.wordpress.org/Plugin_API
 */

/*
 * Set up the content width value based on the theme's design.
 *
 */

 /**
  * Define Constants
  */
 define( 'AAZEEN_THEME_VERSION', '1.2.0' );
 define( 'AAZEEN_THEME_SETTINGS', 'aazeen-settings' );

if (! function_exists('aazeen_setup')) :
//**************aazeen SETUP******************//
function aazeen_setup()
{

  // Set the default content width.
	$GLOBALS['content_width'] = 720;

/*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
    add_theme_support('title-tag');


    // Add default posts and comments RSS feed links to head.
    add_theme_support('automatic-feed-links');

    //Register Menus
    register_nav_menus(array(
        'primary' => __('Primary Navigation(Header)', 'aazeen'),
    ));

    // Declare WooCommerce support
    add_theme_support('woocommerce');

		// Add theme support for woocommerce product gallery added in WooCommerce 3.0.
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-slider' );

    // Add theme support for SiteOrigin Page Builder.
    add_theme_support( 'siteorigin-panels', array(
      'margin-bottom'         => 0,
      'recommended-widgets' 	=> false,
    ) );

    //Custom Background
    $args = array(
    'default-color' => 'f7f7f7',
);
    add_theme_support('custom-background', $args);

    /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
    add_theme_support('html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ));

    /*
         * Enable support for custom Header image.
         *
         *  @since advance
         */
    $args = array(
            'flex-width'    => true,
            'flex-height'   => true,
            'header-text'   => false,
    );
    add_theme_support('custom-header', $args);

    //Post Thumbnail
    add_theme_support('post-thumbnails');

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Add theme support for selective refresh for widgets.
    add_theme_support('customize-selective-refresh-widgets');
    /*

    /*
         * Enable support for custom logo.
         *
         *  @since aazeen
         */


    $defaults = array(
        'height'      => 100,
        'width'      => 220,
        'flex-width'  => true,
				'flex-height'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    );
    add_theme_support('custom-logo', $defaults);

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    // Add featured image sizes
    //
    // Sizes are optimized and cropped for landscape aspect ratio
    // and optimized for HiDPI displays on 'small' and 'medium' screen sizes.
    add_image_size('aazeen-small', 540, 370, true); // name, width, height, crop
    add_image_size('aazeen-medium', 750, 450, true);
		add_image_size('aazeen-post-layout1', 400, 275, true);
    add_image_size('aazeen-large', 1200, 500, true);
    add_image_size('aazeen-xlarge', 1920, 600, true);
		add_image_size('aazeen-slider', 1440, 500, true);

		if ( class_exists( 'woocommerce' ) ) {
			add_image_size( 'aazeen-shop', 300, 300, true );
			add_image_size( 'aazeen-shop-2x', 460, 700, true );
		}
    add_theme_support('starter-content', array(

    'posts' => array(
        'home',
        'blog' ,
    ),

        'options' => array(
            'show_on_front' => 'page',
            'page_on_front' => '{{home}}',
            'page_for_posts' => '{{blog}}',
        ),


        'nav_menus' => array(
            'primary' => array(
                'name' => __('Primary Navigation(Header)', 'aazeen'),
                'items' => array(
                    'page_home',
                    'page_about',
                    'page_blog',
                    'page_contact',
                ),
            ),
        ),
    ));
}
endif; // aazeen_setup
add_action('after_setup_theme', 'aazeen_setup');



//Load CSS files

function aazeen_scripts()
{
    wp_enqueue_style('fontawesome', get_template_directory_uri() . '/css/fonts/css/font-awesome.min.css', 'font_awesome', true);
    wp_enqueue_style('aazeen_core', get_template_directory_uri() . '/css/aazeen.min.css', 'aazeencore_css', true);
    wp_enqueue_style('aazeen-fonts', aazeen_fonts_url(), array(), null);
    wp_enqueue_style('aazeen-style', get_stylesheet_uri());
}
add_action('wp_enqueue_scripts', 'aazeen_scripts');


/**
 * Google Fonts
 */

function aazeen_fonts_url()
{
    $fonts_url = '';

    /* Translators: If there are characters in aazeenr language that are not
    * supported by Lato, translate this to 'off'. Do not translate
    * into aazeenr own language.
    */
    $lato = _x('on', 'Lato font: on or off', 'aazeen');

    /* Translators: If there are characters in your language that are not
    * supported by Ubuntu, translate this to 'off'. Do not translate
    * into your own language.
    */
    $ubuntu = _x('on', 'Ubuntu font: on or off', 'aazeen');



    /* Translators: If there are characters in your language that are not
    * supported by Lora, translate this to 'off'. Do not translate
    * into your own language.
    */
    $open_sans = _x('on', 'Open Sans font: on or off', 'aazeen');

    if ('off' !== $lato || 'off' !== $ubuntu || 'off' !== aazeen) {
        $font_families = array();

        if ('off' !== $ubuntu) {
            $font_families[] = 'Ubuntu:400,500,700';
        }

        if ('off' !== $lato) {
            $font_families[] = 'Lato:400,700,400italic,700italic';
        }

        if ('off' !== $open_sans) {
            $font_families[] = 'Open Sans:400,400italic,700';
        }

        $query_args = array(
                'family' => urlencode(implode('|', $font_families)),
                'subset' => urlencode('latin,latin-ext'),
            );

        $fonts_url = add_query_arg($query_args, '//fonts.googleapis.com/css');
    }

    return $fonts_url;
}

//Load Java Scripts
function aazeen_head_js()
{
    if (!is_admin()) {
        wp_enqueue_script('jquery');
        wp_enqueue_script('aazeen_js', get_template_directory_uri().'/js/aazeen.min.js', array('jquery'), true);
        wp_enqueue_script('aazeen_other', get_template_directory_uri().'/js/aazeen_other.min.js', array('jquery'), true);

        if (is_singular()) {
            wp_enqueue_script('comment-reply');
        }
    }
}
add_action('wp_enqueue_scripts', 'aazeen_head_js');


/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function aazeen_widgets_init()
{

    register_sidebar(array(
    'name'          => __('Right Sidebar', 'aazeen'),
    'id'            => 'right-sidebar',
    'description'   => __('Right Sidebar', 'aazeen'),
    'before_widget' => '<div id="%1$s" class="widget %2$s sidebar-item cell small-24 medium-12 large-24"><div class="widget_wrap ">',
    'after_widget'  => '</div></div>',
    'before_title'  => '<div class="widget-title "> <h3>',
    'after_title'   => '</h3></div>'
    ));
		register_sidebar(array(
		'name'          => __('Home widgets area', 'aazeen'),
		'id'            => 'home-sidebar',
		'description'   => __('Home widgets area you can use any Aazeen custom widgets', 'aazeen'),
		'before_widget' => '<div id="%1$s" class="widget %2$s ">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => ''
		));

		register_sidebar(array(
		'name'          => __('WooCommerce sidebar Widgets', 'aazeen'),
		'id'            => 'woocommerce-sidebar-aazeen',
		'description'   => __('Home Right Sidebar', 'aazeen'),
		'before_widget' => '<div id="%1$s" class="widget %2$s sidebar-item cell small-24 medium-12 large-24"><div class="widget_wrap woocommerce_sidebar ">',
		'after_widget'  => '</div></div>',
		'before_title'  => '<div class="widget-title"> <h3>',
		'after_title'   => '</h3></div>'
		));
		register_sidebar(array(
		'name'          => __('Footer Widgets Full', 'aazeen'),
		'id'            => 'foot_sidebar_full',
		'description'   => __('Widget Area for the Footer full (one )', 'aazeen'),
		'before_widget' => '<div id="%1$s" class="widget %2$s widget_wrap footer_widgets_warp " ><aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside></div>',
		'before_title'  => '<div class="widget-title "> <h3>',
		'after_title'   => '</h3></div>'
		));
    register_sidebar(array(
    'name'          => __('Footer Widgets', 'aazeen'),
    'id'            => 'foot_sidebar',
    'description'   => __('Widget Area for the Footer', 'aazeen'),
    'before_widget' => '<div id="%1$s" class="widget %2$s widget_wrap footer_widgets_warp cell small-24 medium-12 large-auto align-self-top " ><aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside></div>',
    'before_title'  => '<div class="widget-title "> <h3>',
    'after_title'   => '</h3></div>'
    ));
}

add_action('widgets_init', 'aazeen_widgets_init');

/**
 * Checks whether woocommerce is active or not
 *
 * @return boolean
 */
function aazeen_is_woocommerce_active() {
	if ( class_exists( 'woocommerce' ) ) {
		return true;
	} else {
		return false;
	}
}


if ( ! function_exists( 'rwmb_meta' ) ) {
    function rwmb_meta( ) {
        return false;
    }
}
/**
 * If woocommerce is active, load compatibility file
 */
if ( aazeen_is_woocommerce_active() ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

/** Register all navigation menus */
require_once(get_template_directory() . '/functions/menu.php');

/** Image function */
require_once(get_template_directory() . '/functions/function-hooks.php');

/** color function */
require_once(get_template_directory() . '/functions/custom-css.php');

/** Layout function */
require_once(get_template_directory() . '/functions/template-layout.php');

/**
 * Implement the Custom Header feature.
 */
require_once(get_template_directory() . '/inc/custom-header.php');
require_once(get_template_directory() . '/inc/tgmplugin.php');


//load widgets ,kirki ,customizer,functions
require_once(get_template_directory() . '/inc/kirki/kirki.php');
require_once(get_template_directory() . '/inc/customizer.php');
