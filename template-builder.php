<?php
/*
Template Name: Page builder
*/
?>
<?php get_header(); ?>
<!--Call Sub Header-->

<!--Content-->
<div id="content-page-builder" >
          <?php if(have_posts()): ?>
            <?php while(have_posts()): ?>
              <?php the_post();?>
              <div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
                <?php the_content();
                wp_link_pages( array(
                  'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'aazeen' ) . '</span>',
                  'after'       => '</div>',
                  'link_before' => '<span>',
                  'link_after'  => '</span>',
                  'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'aazeen' ) . ' </span>%',
                  'separator'   => '<span class="screen-reader-text">, </span>',
                ) );
                ?>

            <?php endwhile ?>
            </div>
        <?php endif ;?>
  </div>
<?php get_footer(); ?>
