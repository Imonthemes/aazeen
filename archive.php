
<?php get_header();
/**
 * The template for displaying archive
 *
 * themezwp
 * @subpackage aazeen
 * @since aazeen 1.0
 */
 ?>
 <?php if ( true == get_theme_mod( 'aazeen_subheader_onof', true ) ) : ?>
 <!--Call Sub Header-->
  <?php
   echo '<div id="sub_banner" >';
   echo '<div class="top-bar">';
   echo '<div class="top-bar-left">';
   echo '<div class="top-bar-title">';
   echo  the_archive_title( '<h2 class="subheader">', '</h2>' );
   echo '</div>';
   echo '</div>';
   echo '<div class="top-bar-right">';
   echo '<div class="breadcrumb-wrap">';
   echo aazeen_breadcrumb();
   echo '</div>';
   echo '</div>';
   echo '</div>';
   echo '</div>';
 ?>
 <!--Call Sub Header-->
 <?php endif; ?>
 <div id="blog-content" class="blog-warp">
     <?php get_template_part( 'template-parts/main-post', 'loop',get_post_format() );?>
 </div><!--container END-->

<?php get_footer(); ?>
