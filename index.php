<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package aazeen
 */

get_header(); ?>

<?php if ( true == get_theme_mod( 'aazeen_subheader_onof', true ) ) : ?>
<?php
if ( !is_front_page() && is_home() ) {
  // blog page
  echo aazeen_mainpost_blog_title();
}
?>
<?php endif; ?>
<div id="blog-content" class="blog-warp">
    <?php get_template_part( 'template-parts/main-post', 'loop',get_post_format() );?>
</div><!--container END-->

<?php get_footer(); ?>
