<?php get_header();?>
<?php $value = rwmb_meta( 'on_off_pagesub');
?>
<!--Call Sub Header-->
<?php if ($value == true) : ?>
<div id="sub_banner_page" class=" callout  border-none">
  <?php if ( has_post_thumbnail( $post->ID ) ) : ?>
  <div class="single-page-thumb-outer">
    <div class="page-thumb" data-interchange="[<?php echo the_post_thumbnail_url('aazeen-small'); ?>, small], [<?php echo the_post_thumbnail_url('aazeen-large'); ?>, medium], [<?php echo the_post_thumbnail_url('aazeen-xlarge'); ?>, large], [<?php echo the_post_thumbnail_url('aazeen-xlarge'); ?>, xlarge]">
        <h1 class="text-center">
          <?php the_title(); ?>
        </h1>
    </div>
  </div>
<?php else:?>
  <?php
    $value_gradient = rwmb_meta( 'select_gradient_page_subheader' );
  ?>
      <div class="heade-page-nothumb <?php if (! $value_gradient) : ?> gradient_12 <?php else:?> gradient_<?php echo esc_attr($value_gradient);?> <?php endif;?>" >
        <h1 class="text-center">
          <?php the_title(); ?>
        </h1>
    </div>
<?php endif;?>
</div>
<?php else :?>
<?php //nothing ?>
<?php endif;?>

<!--Content-->
<div id="content-page" class="padding-vertical-1 <?php if ($value == false) : ?> no-page-header <?php endif;?> ">
  <div class="grid-container ">
    <div class="grid-x grid-padding-x align-center">
      <div class="cell  small-24 large-auto">
        <div class="page_content ">
          <?php if(have_posts()): ?>
            <?php while(have_posts()): ?>
              <?php the_post();?>
              <div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
                <div class="metadate">
                  <?php
                  edit_post_link(
                    sprintf(
                      /* translators: %s: Name of current post */
                      __( 'Edit<span class="screen-reader-text"> "%s"</span>', 'aazeen' ),
                      get_the_title()
                    ),
                    '<span class="edit-link">',
                    '</span>'
                  );
                  ?>
                </div>

              <div class="page_content_wrap">
                <?php the_content();
                wp_link_pages( array(
                  'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'aazeen' ) . '</span>',
                  'after'       => '</div>',
                  'link_before' => '<span>',
                  'link_after'  => '</span>',
                  'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'aazeen' ) . ' </span>%',
                  'separator'   => '<span class="screen-reader-text">, </span>',
                ) );
                ?>

              </div>
            <?php endwhile ?>

          </div>
          <?php if ( comments_open() || get_comments_number() ) {?>
          <div class="comments_template">
            <?php  comments_template();?>
          </div>
          <?php }?>
        <?php endif ;?>
        </div>
      </div>
      <!--PAGE END-->
      <?php get_template_part('sidebar'); ?>
    </div>
    </div>
  </div>
<?php get_footer(); ?>
